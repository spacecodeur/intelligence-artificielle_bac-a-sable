import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns

link = "https://raw.githubusercontent.com/murpi/wilddata/master/quests/weather2019.csv"
df_weather = pd.read_csv(link)
df_weather = df_weather.iloc[:, [0, 1, 2, -4]]

# Créez une nouvelle figure
plt.figure(figsize=(12, 6))

# Première figure : régression linéaire
plt.subplot(121)  # 1 ligne, 2 colonnes, le premier sous-graphique
sns.regplot(data=df_weather,
            x="SUNHOUR",
            y="MAX_TEMPERATURE_C")
plt.title('Max Temperature related to Sunhour')

# Scikit-Learn peut trouver les coefficients de la régression linéaire
from sklearn.linear_model import LinearRegression

# Séries X et Y
X = df_weather[['SUNHOUR']]
y = df_weather['MAX_TEMPERATURE_C']

# Entraînement du modèle de régression linéaire
modelLR = LinearRegression().fit(X.values, y)

# Coefficient et interception
print("coefficient :", modelLR.coef_)
print("intercept :", modelLR.intercept_)

# Prédiction de la température maximale en fonction des heures de soleil (par exemple, 10 heures)
print("Scikit-Learn :  ", modelLR.predict([[10]]))

# Prédiction de la température maximale pour l'ensemble des heures de soleil
y_maxTempPredicted = modelLR.predict(df_weather[['SUNHOUR']].values)

df_weather['y_maxTempPredicted'] = y_maxTempPredicted

print(df_weather.head())

# Deuxième figure : dispersion (scatterplot)
plt.subplot(122)  # 1 ligne, 2 colonnes, le deuxième sous-graphique

df_weather['DATE'] = pd.to_datetime(df_weather['DATE']) 

# Affiche le graphique des températures maximales réelles en bleu
sns.scatterplot(data=df_weather,
                x="DATE",
                y="MAX_TEMPERATURE_C",
                color='blue',
                label='real')

# Affiche le graphique des températures maximales prédites en rouge
sns.scatterplot(data=df_weather,
                x="DATE",
                y="y_maxTempPredicted",
                color='red',
                label='predicted')

plt.title('Max Temperature C related to Sunhour')
plt.legend()

# Afficher les deux graphiques côte à côte
plt.tight_layout()
plt.show()
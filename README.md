# Intelligence Artificielle : bac à sable

Ce repo contient un ensemble de sous-projets faisant office de gros bac à sable pour faire joujou avec des IA et éventuelles technos sympatiques pour me faire la main

Dans un but pédagogique, les projets sont pour la plupart développés en live sur ma chaîne twitch et les vidéos sont stockées sur ma chaîne youtube : 

- lien vers twitch : https://www.twitch.tv/spacecodeur
- lien vers youtube : https://www.youtube.com/channel/UCJyBS-oJ4HZUPe7UqBdhOTw
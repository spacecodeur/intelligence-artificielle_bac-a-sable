import PieceInterface from './PieceInterface';
import { coordonatesType } from '~/Types/coordonatesType';

abstract class PieceAbstract implements PieceInterface {
    private player: number;

    constructor(player: number) {
        this.player = player;
    }

    setPlayer(player: number): this {
        this.player = player;
        return this;
    }

    getPlayer(): number {
        return this.player;
    }

    abstract toChar(): string;
    abstract getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[];
    abstract isPromotable(): boolean;
}

export default PieceAbstract;
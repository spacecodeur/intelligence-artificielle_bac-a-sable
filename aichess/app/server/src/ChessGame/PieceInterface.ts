import { coordonatesType } from "~/Types/coordonatesType";

interface PieceInterface{
    setPlayer(player: number):this;
    getPlayer():number;
    toChar(): string;
    getPossiblyMoves(currentCoordinate:coordonatesType): Array<coordonatesType>;
    isPromotable(): boolean;
}

export default PieceInterface;
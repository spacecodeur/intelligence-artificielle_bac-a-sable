import PieceAbstract from '../PieceAbstract';
import { coordonatesType } from '~/Types/coordonatesType';

class Knight extends PieceAbstract {
    constructor(player: number) {
        super(player);
    }

    toChar(): string {
        return this.getPlayer() === 0 ? '♞' : '♘';
    }

    getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[] {

        return [{x:0, y:0}];

        // // Implement the logic for possible moves of the Knight
        // const possibleMoves: coordonatesType[] = [];
        
        // const knightMoves = [
        //     { x: -2, y: -1 },
        //     { x: -2, y: 1 },
        //     { x: -1, y: -2 },
        //     { x: -1, y: 2 },
        //     { x: 1, y: -2 },
        //     { x: 1, y: 2 },
        //     { x: 2, y: -1 },
        //     { x: 2, y: 1 },
        // ];

        // for (const move of knightMoves) {
        //     const x = currentCoordinate.x + move.x;
        //     const y = currentCoordinate.y + move.y;

        //     if (x >= 0 && x < 8 && y >= 0 && y < 8) {
        //         possibleMoves.push({ x, y });
        //     }
        // }

        // // You may need to add further checks to ensure the Knight doesn't move through other pieces.

        // return possibleMoves;
    }

    isPromotable(): boolean {
        // Knights are not promotable in standard chess rules
        return false;
    }
}

export default Knight;

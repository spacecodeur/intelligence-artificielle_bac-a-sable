import PieceAbstract from "../PieceAbstract";
import { coordonatesType } from "~/Types/coordonatesType";

class Pawn extends PieceAbstract {
    constructor(player: number) {
        super(player);
    }

    toChar(): string {
        return this.getPlayer() === 0 ? '♟' : '♙';
    }

    getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[] {

        return [{x:0, y:0}];

        // // Implement the logic to get possible moves for the Pawn
        // // Example logic: Pawn can move forward one square (two on its first move)
        // // and capture diagonally
        // const possibleMoves: coordonatesType[] = [];
        // const direction = this.getPlayer() === 0 ? 1 : -1; // Adjust for player direction
        // const x = currentCoordinate.x;
        // const y = currentCoordinate.y;

        // // Move one square forward
        // possibleMoves.push({ x, y: y + direction });

        // // First move: Pawn can move two squares forward
        // if ((this.getPlayer() === 0 && y === 1) || (this.getPlayer() === 1 && y === 6)) {
        //     possibleMoves.push({ x, y: y + 2 * direction });
        // }

        // // Capture diagonally
        // possibleMoves.push({ x: x - 1, y: y + direction });
        // possibleMoves.push({ x: x + 1, y: y + direction });

        // return possibleMoves;
    }

    isPromotable(): boolean {
        return true;
    }
}

export default Pawn;
import PieceAbstract from '../PieceAbstract';
import { coordonatesType } from '~/Types/coordonatesType';

class King extends PieceAbstract {
    constructor(player: number) {
        super(player);
    }

    toChar(): string {
        return this.getPlayer() === 0 ? '♚' : '♔';
    }

    getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[] {
        // Implement the logic for possible moves of the King
        // The King can move one step in any direction (up, down, left, right, and diagonally)
        const possibleMoves: coordonatesType[] = [
            { x: currentCoordinate.x - 1, y: currentCoordinate.y - 1 },
            { x: currentCoordinate.x - 1, y: currentCoordinate.y },
            { x: currentCoordinate.x - 1, y: currentCoordinate.y + 1 },
            { x: currentCoordinate.x, y: currentCoordinate.y - 1 },
            { x: currentCoordinate.x, y: currentCoordinate.y + 1 },
            { x: currentCoordinate.x + 1, y: currentCoordinate.y - 1 },
            { x: currentCoordinate.x + 1, y: currentCoordinate.y },
            { x: currentCoordinate.x + 1, y: currentCoordinate.y + 1 },
        ];

        // You may need to add further checks to ensure the King doesn't go out of bounds or move into check.
        // Make sure to validate the possible moves based on your chess game rules.

        return possibleMoves.filter(move =>
            move.x >= 0 && move.x <= 7 && move.y >= 0 && move.y <= 7
        );
    }

    isPromotable(): boolean {
        // The King is not promotable in standard chess rules
        return false;
    }
}

export default King;

import PieceInterface from "~/ChessGame/PieceInterface";

export type squaresType = Array<Array<PieceInterface|null>>;

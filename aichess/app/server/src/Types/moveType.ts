import { coordonatesType } from "./coordonatesType";
import { positionType } from "./positionType";

export type moveType = {
    start: {
        position: positionType,
        coordonates: coordonatesType
    },
    end: {
        position: positionType,
        coordonates: coordonatesType,
        promotion: string
    }
};
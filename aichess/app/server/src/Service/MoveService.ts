import { moveType } from "~/Types/moveType";
import BoardService from "./board/BoardService";

export default class MoveService
{

    static moveRegex = /([A-H][1-8])-([A-H][1-8])([QRBN])?/;


    public static isWellFormedStringMove(stringMove:string): boolean 
    {
        return MoveService.moveRegex.test(stringMove);
    }

    public static stringMoveToObjectMove(stringMove:string): moveType
    {
        let oMove = stringMove.match(MoveService.moveRegex);
        return {
            start: {
                position: {
                    letter:oMove![1][0],
                    number:oMove![1][1],
                },
                coordonates: BoardService.positionToCoordonates({letter:oMove![1][0], number:oMove![1][1]})
            },
            end: {
                position: {
                    letter:oMove![2][0],
                    number:oMove![2][1],
                },
                coordonates: BoardService.positionToCoordonates({letter:oMove![2][0], number:oMove![2][1]}),
                promotion: oMove![3]
            }
        }
    }
}
import Board from "~/Entity/Board";

export default class BoardPrinterService {

    static printToPostman(Board:Board)
    {
        let prettyPrint = '';
        let iCoordinate = 8;

        for (const row of Board.squares) {
            prettyPrint += `${iCoordinate}\t`;

            for (let square of row) {
                if (null === square) {
                    prettyPrint += `[ ]`;
                }
                else
                {
                    // ici j'accède à chaque case de mon échiquier (square)
                    prettyPrint += `[${square.toChar()}]`;
                }
            }

            prettyPrint += '\n';

            iCoordinate -= 1;
        }

        prettyPrint += '\n';
        prettyPrint += '\t A  B  C  D  E  F  G  H';

        return prettyPrint;
    }
}
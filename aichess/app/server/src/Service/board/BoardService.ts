import type { positionType } from '~/Types/positionType';
import type { coordonatesType } from '~/Types/coordonatesType';
import { Variable } from '~/ToolsBox/variable';

import Board from '~/Entity/Board';
import Pawn from '~/ChessGame/Piece/Pawn';
import Rook from '~/ChessGame/Piece/Rook';
import Knight from '~/ChessGame/Piece/Knight';
import Bishop from '~/ChessGame/Piece/Bishop';
import Queen from '~/ChessGame/Piece/Queen';
import King from '~/ChessGame/Piece/King';
import PieceInterface from '~/ChessGame/PieceInterface';
import { moveType } from '~/Types/moveType';


export default class BoardService {

    static chessBoardCoor = {
        x: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
        y: ['8', '7', '6', '5', '4', '3', '2', '1'],
    }

    public static positionToCoordonates(position: positionType): coordonatesType 
    {
        return {
            x: this.chessBoardCoor.y.indexOf(position.number),
            y: this.chessBoardCoor.x.indexOf(position.letter.toUpperCase())
        }
    }

    public static coordonatesToPosition(coordonates: coordonatesType): positionType 
    {
        return {
            letter: this.chessBoardCoor.x[coordonates.y],
            number: this.chessBoardCoor.y[coordonates.x]
        }
    }

    public static newBoard(buildEmptyBoard:boolean = false): Board
    {

        let eBoard = new Board([
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
        ]);

        if(true === buildEmptyBoard)
        {
            return eBoard;
        }
        else
        {
            // on place les pions
            ['2', '7'].forEach((number, player) => {
                ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Pawn(player), 
                        BoardService.positionToCoordonates({letter, number})
                    )
                });
            });
    
            // on place les pièces
            ['1', '8'].forEach((number, player) => {
                ['a', 'h'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Rook(player), 
                        BoardService.positionToCoordonates({letter, number})
                    );
                });
                ['b', 'g'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Knight(player), 
                        BoardService.positionToCoordonates({letter, number})
                    );
                });
                ['c', 'f'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Bishop(player), 
                        BoardService.positionToCoordonates({letter, number})
                    );
                });
                eBoard.fillSquare(
                    new Queen(player),
                    BoardService.positionToCoordonates({letter: 'd', number})
                );
                eBoard.fillSquare(
                    new King(player),
                    BoardService.positionToCoordonates({letter: 'e', number})
                );
            });

            return eBoard;
        }
    }

    public static loadBoardFromJsonString(sJson: string): Board
    {
        Variable.mustBeJsonString(sJson);
        let flatBoard: Array<Array<string>> = JSON.parse(sJson);
        let Pieces: Array<PieceInterface> = [
            new Pawn(0),new Pawn(1),
            new Rook(0),new Rook(1),
            new Knight(0),new Knight(1),
            new Bishop(0),new Bishop(1),
            new King(0),new King(1),
            new Queen(0),new Queen(1),
        ];
        let PieceFound: PieceInterface|undefined;
        let board = BoardService.newBoard(true);

        for(let rowIndex = 0 ; rowIndex < flatBoard.length ; rowIndex++ )
        {
            for(let columnIndex = 0 ; columnIndex < flatBoard[rowIndex].length ; columnIndex++ )
            {
                if(flatBoard[rowIndex][columnIndex] !== '')
                {
                    PieceFound = Pieces.find((element) => element.toChar() === flatBoard[rowIndex][columnIndex]); // find method return undefined when searched pattern was not found
                    if(PieceFound === undefined)
                    {
                        throw new Error('Piece in flatboard (\'' + flatBoard[rowIndex][columnIndex] + '\') not found in defined Pieces objects');
                    }
                    board.squares[rowIndex][columnIndex] = PieceFound;
                }
            }
        }
        
        return board;
    }

    public static boardToJsonString(board: Board): string
    {
        let flatBoard: Array<Array<string>> = [];
        let Piece;
        for(let rowIndex = 0 ; rowIndex < board.squares.length ; rowIndex++ )
        {
            flatBoard.push([]);

            for(let columnIndex = 0 ; columnIndex < board.squares[rowIndex].length ; columnIndex++ )
            {
                Piece = board.squares[rowIndex][columnIndex];
                if(Piece === null)
                {
                    flatBoard[rowIndex].push('');
                }
                else
                {
                    flatBoard[rowIndex].push(Piece.toChar());
                }
            }
        }
        return JSON.stringify(flatBoard);
    }

    public static isValidMove(Board: Board, Piece: PieceInterface, move: moveType): boolean
    {
        return Piece.getPossiblyMoves(move.start.coordonates).find((coordonates) =>
            coordonates.x === move.end.coordonates.x && coordonates.y === move.end.coordonates.y
        ) !== undefined
    }
}
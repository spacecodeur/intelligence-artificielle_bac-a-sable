import { squaresType } from "~/Types/squaresType"
import { coordonatesType } from "~/Types/coordonatesType";
import PieceInterface from "~/ChessGame/PieceInterface";

export default class Board
{
    squares:squaresType;

    constructor(squares:squaresType)
    {
        this.squares = squares;
    }

    public fillSquare(
        Piece: PieceInterface,
        coordonates: coordonatesType
    ): Board
    {
        this.squares[coordonates.x][coordonates.y] = Piece;

        return this;
    }

    public emptySquare(
        coordonates: coordonatesType
    ): Board
    {
        this.squares[coordonates.x][coordonates.y] = null;

        return this;
    }

    public loadPieceFromCoordonates(
        coordonates: coordonatesType
    ): PieceInterface|null
    {
        return this.squares[coordonates.x][coordonates.y];
    }

}
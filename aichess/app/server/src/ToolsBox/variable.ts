const Variable = {
    isAnInteger: (parameter: number): boolean => Number.isInteger(parameter),

    mustBeAnInteger: (parameter: number): true => {
        if (!Variable.isAnInteger(parameter)) {
            throw new TypeError(
                `the parameter is not an integer as expected (currently value is : ${parameter} )`
            );
        }

        return true;
    },

    isNumericInteger: (parameter: string): boolean => 
    {
        return /^-?\d+$/.test(parameter);
    },

    mustBeNumericInteger: (parameter: string): boolean => 
    {
        if (!Variable.isNumericInteger(parameter)) {
            throw new TypeError(
                `the parameter is a string but does not contain exactly an Integer (currently value is : ${parameter} )`
            );
        }

        return true;
    },

    mustBeAnIntegerGreaterThan: (
        integer: number,
        mustBeGreaterThan: number
    ): boolean => {
        Variable.mustBeAnInteger(integer);

        if (integer <= mustBeGreaterThan) {
            throw new TypeError(
                `the integer ${integer.toString()} is not greater than ${mustBeGreaterThan}`
            );
        }

        return true;
    },

    isAChar: (parameter: string): boolean => 1 === parameter.length,

    mustBeAChar: (parameter: string): true => {
        if (!Variable.isAChar(parameter)) {
            throw new TypeError(
                `the parameter is not a char as expected (currently value is : ${parameter} )`
            );
        }

        return true;
    },

    isALetter: (parameter: string): boolean => {
        Variable.mustBeAChar(parameter);

        return [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
        ].includes(parameter);
    },

    mustBeALetter: (parameter: string): true => {
        if (!Variable.isALetter(parameter)) {
            throw new TypeError(
                `the parameter with the value '${parameter}' is not a letter`
            );
        }

        return true;
    },

    areEqual: (
        parameter1: string | number | boolean,
        parameter2: string | number | boolean
    ): boolean => parameter1 === parameter2,

    mustBeEqual: (
        parameter1: string | number,
        parameter2: string | number
    ): true => {
        if (!Variable.areEqual(parameter1, parameter2)) {
            throw new TypeError(
                `parameter1 and parameter2 are not equal as expected : ${parameter1} (${typeof parameter1}) !== ${parameter2} (${typeof parameter1})`
            );
        }

        return true;
    },

    areSameType: (
        parameter1: string | number | boolean,
        parameter2: string | number | boolean
    ): boolean => typeof parameter1 === typeof parameter2,

    mustBeSameType: (
        parameter1: string | number | boolean,
        parameter2: string | number | boolean
    ): true => {
        if (!Variable.areSameType(parameter1, parameter2)) {
            throw new TypeError(
                'parameter1 and parameter2 are not the same type : '
                    + `typeof parameter1 (${typeof parameter1}) !== typeof parameter2 (${typeof parameter2})`
            );
        }

        return true;
    },

    isLessThan: (
        parameter1: string | number,
        parameter2: string | number
    ): boolean | undefined => {
        Variable.mustBeSameType(parameter1, parameter2);

        if ('number' === typeof parameter1 && 'number' === typeof parameter2) {
            return parameter1 < parameter2;
        }
        if ('string' === typeof parameter1 && 'string' === typeof parameter2) {
            return 0 > parameter1.localeCompare(parameter2);
        }
    },

    mustBeLessThan: (
        parameter1: string | number,
        parameter2: string | number
    ): true => {
        if (!Variable.isLessThan(parameter1, parameter2)) {
            throw new TypeError(
                `parameter1 (${parameter1}) is not lower than parameter2 (${parameter2})`
            );
        }

        return true;
    },

    isLessThanOrEqual: (
        parameter1: string | number,
        parameter2: string | number
    ): boolean | undefined => {
        Variable.mustBeSameType(parameter1, parameter2);

        if ('number' === typeof parameter1 && 'number' === typeof parameter2) {
            return parameter1 <= parameter2;
        }
        if ('string' === typeof parameter1 && 'string' === typeof parameter2) {
            return 0 >= parameter1.localeCompare(parameter2);
        }
    },

    mustBeLessThanOrEqual: (
        parameter1: string | number,
        parameter2: string | number
    ): true => {
        if (!Variable.isLessThanOrEqual(parameter1, parameter2)) {
            throw new TypeError(
                `parameter1 (${parameter1}) is not lower (or equal) than parameter2 (${parameter2})`
            );
        }

        return true;
    },

    isNonEmptyObject: (obj: object): boolean => 0 < Object.keys(obj).length,

    mustBeNonEmptyObject: (obj: object): true => {
        if (!Variable.isNonEmptyObject(obj)) {
            throw new TypeError('The object is empty');
        }

        return true;
    },

    isJsonString: (string:string): boolean => 
    {
        try {
            JSON.parse(string);
            return true;
        } catch (error) {
            return false;
        } 
    },

    mustBeJsonString: (string:string): true => 
    {
        if (!Variable.isJsonString(string)) {
            throw new TypeError('The passed string is not a well formed JSON');
        }

        return true;
    },

    isDefined: (data:any): boolean =>
    {
        return undefined !== data;
    },

    mustBeDefined: (data:any): true =>
    {
        if (!Variable.isDefined(data)) {
            throw new TypeError('The passed data is not defined');
        }

        return true;
    }
};

export { Variable };

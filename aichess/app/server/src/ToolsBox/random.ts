import { Variable } from './variable';

const Random = {
    pickRandPropertyOfObject: (obj: object): string => {
        Variable.mustBeNonEmptyObject(obj);

        return Object.keys(obj)[
            Math.floor(Math.random() * Object.keys(obj).length)
        ];
    },

    pickRandEntryOfArray: (tab: Array<any>): any =>
        tab[Math.floor(Math.random() * tab.length)],

    pickRandLetterInRange: (
        minLetterInclusive: string,
        maxLetterInclusive: string
    ): string => {
        Variable.mustBeAChar(minLetterInclusive);
        Variable.mustBeAChar(maxLetterInclusive);
        Variable.mustBeLessThanOrEqual(minLetterInclusive, maxLetterInclusive);

        const alphabet = 'abcdefghijklmnopqrstuvwxyz';

        const range = [];
        for (const letter of alphabet) {
            if (letter === minLetterInclusive || 0 !== range.length) {
                range.push(letter);
            }

            if (letter === maxLetterInclusive) {
                break;
            }
        }

        return Random.pickRandEntryOfArray(range);
    },

    pickRandInt: (
        minIntegerInclusive: number,
        maxIntegerExclusive: number
    ): number => {
        Variable.mustBeAnInteger(minIntegerInclusive);
        Variable.mustBeAnInteger(maxIntegerExclusive);
        Variable.mustBeLessThan(minIntegerInclusive, maxIntegerExclusive);

        return Math.floor(
            Math.random() * (maxIntegerExclusive - minIntegerInclusive) +
                minIntegerInclusive
        );
    },

    generateRandomSimpleArrayOfIntegersInRange: (
        nbrElements: number,
        minIntegerInclusive: number,
        maxIntegerExclusive: number
    ): Array<number> => {
        Variable.mustBeAnIntegerGreaterThan(nbrElements, 0);
        Variable.mustBeAnInteger(minIntegerInclusive);
        Variable.mustBeAnInteger(maxIntegerExclusive);
        Variable.mustBeLessThan(minIntegerInclusive, maxIntegerExclusive);

        return Array.from(
            { length: nbrElements },
            () => Math.floor(
                    Math.random() * (maxIntegerExclusive - minIntegerInclusive)
                ) + minIntegerInclusive
        );
    },
};

export { Random };

import { Random } from '../random';

describe('check functions', () => {
    test('function pickRandInt', () => {
        expect([10, 11, 12]).toContain(Random.pickRandInt(10, 13));
        expect(Random.pickRandInt(10, 11)).toBe(10);
    });

    test('function pickRandLetterInRange', () => {
        expect(Random.pickRandLetterInRange('a', 'a')).toBe('a');
        expect(Random.pickRandLetterInRange('z', 'z')).toBe('z');
        expect(['e', 'f', 'g', 'h']).toContain(
            Random.pickRandLetterInRange('e', 'h')
        );
    });

    test('function generateRandomSimpleArrayOfIntegersInRange', () => {
        const randInt = Random.pickRandInt(100, 200);
        const randArray1 = Random.generateRandomSimpleArrayOfIntegersInRange(
            randInt,
            45,
            50
        );

        expect(randArray1.length).toBe(randInt);
        randArray1.forEach((element) => {
            expect(element).toBeLessThan(50);
            expect(element).toBeGreaterThanOrEqual(45);
        });
    });

    test('function pickRandPropertyOfObject', () => {
        const object = {
            faa: 11,
            fee: 22,
            fii: 33,
            foo: 44,
            fuu: 55,
        };
        const element = Random.pickRandPropertyOfObject(object);

        expect(object.hasOwnProperty(element)).toBe(true);
        expect(object.hasOwnProperty('fyy')).toBe(false);
    });

    test('function pickRandEntryOfArray', () => {
        const randInt = Random.pickRandInt(1, 5);
        const randArray1 = Random.generateRandomSimpleArrayOfIntegersInRange(
            randInt,
            1,
            1000000
        );

        const element = Random.pickRandEntryOfArray(randArray1);
        expect(randArray1).toContain(element);

        const randArray2 = Random.generateRandomSimpleArrayOfIntegersInRange(
            1,
            1,
            2
        );
        expect(randArray2).toStrictEqual([1]);
    });
});

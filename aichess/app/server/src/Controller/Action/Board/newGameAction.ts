import { Request, Response } from "express";
import Board from "~/Entity/Board";
import BoardService from "~/Service/board/BoardService";
import BoardPrinterService from "~/Service/board/BoardPrinterService";
import King from "~/ChessGame/Piece/King";

const newGameAction = (request: Request, response: Response) => {
    const board:Board = BoardService.newBoard();
    // response.send(BoardPrinterService.printToPostman(board));
    return response.send(BoardService.boardToJsonString(board));
}

const newGameDebugKingAction = (request: Request, response: Response) => {
    const board:Board = BoardService.newBoard(true);
    board.fillSquare(new King(0), BoardService.positionToCoordonates({letter:'d', number:'5'}));

    // response.send(BoardPrinterService.printToPostman(board));
    return response.send(BoardService.boardToJsonString(board));
}

const newGameDebugKingPostmanAction = (request: Request, response: Response) => {
    const board:Board = BoardService.newBoard(true);
    board.fillSquare(new King(0), BoardService.positionToCoordonates({letter:'d', number:'5'}));

    return response.send(BoardPrinterService.printToPostman(board));
}

export {
    newGameAction,
    newGameDebugKingAction,
    newGameDebugKingPostmanAction,
};
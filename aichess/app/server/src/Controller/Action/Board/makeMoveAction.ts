import { Request, Response } from "express";
import PieceInterface from "~/ChessGame/PieceInterface";
import Board from "~/Entity/Board";
import MoveService from "~/Service/MoveService";
import BoardService from "~/Service/board/BoardService";
import { moveType } from "~/Types/moveType";
import { Variable } from "~/ToolsBox/variable";
import { coordonatesType } from "~/Types/coordonatesType";
import BoardPrinterService from "~/Service/board/BoardPrinterService";

const makeMoveAction = (request: Request, response: Response) => {

    let move:moveType = MoveService.stringMoveToObjectMove(request.body.move);

    let Board:Board = BoardService.loadBoardFromJsonString(request.body.board);

    let player = request.body.player;
    Variable.mustBeNumericInteger(player);
    player = parseInt(player);

    let Piece:PieceInterface|null = Board.loadPieceFromCoordonates(move.start.coordonates);

    if(Piece === null)
    {
        return response.send('No piece found on position : ' + move.start.position.letter + move.start.position.number);
    }

    if(Piece.getPlayer() !== player)
    {
        return response.send('Piece found on board (\'' + Piece.getPlayer() + '\'), but wrong current player (\'' + player + '\')');
    }

    if(BoardService.isValidMove(Board, Piece, move) !== true)
    {
        return response.send('The piece ' + Piece.toChar() + ' can\'t move to ' + move.end.position.letter + move.end.position.number + ' (possiblyMoves are : \'' + 
            Piece.getPossiblyMoves(move.start.coordonates).map((possiblyMove:coordonatesType) => {
                let position = BoardService.coordonatesToPosition(possiblyMove);
                return position.letter + position.number
            }).join(',')
        + '\')');
    }

    Board.emptySquare(move.start.coordonates);
    Board.fillSquare(Piece, move.end.coordonates);

    // return response.send(BoardService.boardToJsonString(Board));
    return response.send(BoardPrinterService.printToPostman(Board));

}

export default makeMoveAction;

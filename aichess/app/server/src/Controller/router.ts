import { Router } from 'express';

import makeMoveAction from './Action/Board/makeMoveAction';
import makeMoveValidator from './Validator/makeMoveValidator';
import { newGameAction, newGameDebugKingAction, newGameDebugKingPostmanAction} from './Action/Board/newGameAction';


const router = Router();

// Board Actions
router.get( '/'     ,                           newGameAction);
router.get( '/dk'   ,                           newGameDebugKingAction);
router.get( '/dkp'  ,                           newGameDebugKingPostmanAction);
router.post('/move' , ...makeMoveValidator ,    makeMoveAction); // not warAction

export default router;
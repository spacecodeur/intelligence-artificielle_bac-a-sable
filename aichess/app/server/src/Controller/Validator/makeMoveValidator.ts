import requestParamBoardCheck from "./Check/requestParamBoardCheck";
import requestParamMoveCheck from "./Check/requestParamMoveCheck";
import requestParamPlayerCheck from "./Check/requestParamPlayerCheck";

const makeMoveValidator = [
    requestParamMoveCheck,
    requestParamBoardCheck,
    requestParamPlayerCheck
]

export default makeMoveValidator;
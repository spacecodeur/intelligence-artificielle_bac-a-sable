import { Request, Response, NextFunction } from "express";
import { Variable } from "~/ToolsBox/variable";

const requestParamBoardCheck = (request: Request, response: Response, next: NextFunction): void|Response => 
{
    try
    {
        Variable.mustBeDefined(request.body.board);
    }
    catch (e)
    {
        if(e instanceof TypeError)
        {
            return response.status(400).send('board param missing');
        }
        else
        {
            throw Error('Exception trigged unexpected');
        }
    }

    try
    {
        Variable.mustBeJsonString(request.body.board);
    }
    catch (e)
    {
        if(e instanceof TypeError)
        {
            return response.status(400).send('bad board param format');
        }
        else
        {
            throw Error('Exception trigged unexpected');
        }
    }

    return next();
}

export default requestParamBoardCheck;
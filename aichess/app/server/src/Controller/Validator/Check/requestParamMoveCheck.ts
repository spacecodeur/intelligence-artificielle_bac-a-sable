import { Request, Response, NextFunction } from "express";
import MoveService from "~/Service/MoveService";
import { Variable } from "~/ToolsBox/variable";


const requestParamMoveCheck = (request: Request, response: Response, next: NextFunction): void|Response => 
{
    try
    {
        Variable.mustBeDefined(request.body.move);
        if(false === MoveService.isWellFormedStringMove(request.body.move))
        {
            return response.status(400).send('bad move param format');
        }
    }
    catch(e)
    {
        if(e instanceof TypeError)
        {
            return response.status(400).send('move param missing');
        }
        else
        {
            throw Error('Exception trigged unexpected');
        }
    }

    return next();
}

export default requestParamMoveCheck;
import Bishop from "~/ChessGame/Piece/Bishop";

// describe('Bishop Class', () => {
//   // Test the toChar() method
//   describe('toChar()', () => {
//     it('should return ♝ for player 0', () => {
//       const bishop = new Bishop(0);
//       expect(bishop.toChar()).toBe('♝');
//     });

//     it('should return ♗ for player 1', () => {
//       const bishop = new Bishop(1);
//       expect(bishop.toChar()).toBe('♗');
//     });
//   });

//   // Define the expected number of possible moves for each square
//   const expectedMoves = [
//     [7, 7, 7, 7, 7, 7, 7, 7],
//     [7, 9, 9, 9, 9, 9, 9, 7],
//     [7, 9, 11, 11, 11, 11, 9, 7],
//     [7, 9, 11, 13, 13, 11, 9, 7],
//     [7, 9, 11, 13, 13, 11, 9, 7],
//     [7, 9, 11, 11, 11, 11, 9, 7],
//     [7, 9, 9, 9, 9, 9, 9, 7],
//     [7, 7, 7, 7, 7, 7, 7, 7],
//   ];

//   // Test the getPossiblyMoves() method for all squares
//   for (let x = 0; x < 8; x++) {
//     for (let y = 0; y < 8; y++) {
//       it(`should return the correct number of possible moves for square (${x}, ${y})`, () => {
//         const bishop = new Bishop(0);
//         const currentPosition = { x, y };
//         const possibleMoves = bishop.getPossiblyMoves(currentPosition);

//         const expectedMoveCount = expectedMoves[x][y];

//         expect(possibleMoves).toHaveLength(expectedMoveCount);
//       });
//     }
//   }

//   // Test the isPromotable() method
//   describe('isPromotable()', () => {
//     it('should return false for isPromotable', () => {
//       const bishop = new Bishop(0);
//       expect(bishop.isPromotable()).toBe(false);
//     });
//   });
// });

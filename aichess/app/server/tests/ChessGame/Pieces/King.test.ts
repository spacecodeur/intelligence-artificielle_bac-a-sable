import King from "~/ChessGame/Piece/King";
import BoardService from "~/Service/board/BoardService";
import { coordonatesType } from "~/Types/coordonatesType";
import { positionType } from "~/Types/positionType";

describe('King Class', () => {
    // Test the toChar() method
    test('should return ♚ for player 0', () => {
        const king = new King(0);
        expect(king.toChar()).toBe('♚');
    });

    test('should return ♔ for player 1', () => {
        const king = new King(1);
        expect(king.toChar()).toBe('♔');
    });

    // Define the expected number of possible moves for each square
    const expectedPossiblyMoves = [
        [
            ["A7", "B7", "B8"],
            ["A7", "A8", "B7", "C7", "C8"],
            ["B7", "B8", "C7", "D7", "D8"],
            ["C7", "C8", "D7", "E7", "E8"],
            ["D7", "D8", "E7", "F7", "F8"],
            ["E7", "E8", "F7", "G7", "G8"],
            ["F7", "F8", "G7", "H7", "H8"],
            ["G7", "G8", "H7"],
        ],
        [
            ["A6", "A8", "B6", "B7", "B8"],
            ["A6", "A7", "A8", "B6", "B8", "C6", "C7", "C8"],
            ["B6", "B7", "B8", "C6", "C8", "D6", "D7", "D8"],
            ["C6", "C7", "C8", "D6", "D8", "E6", "E7", "E8"],
            ["D6", "D7", "D8", "E6", "E8", "F6", "F7", "F8"],
            ["E6", "E7", "E8", "F6", "F8", "G6", "G7", "G8"],
            ["F6", "F7", "F8", "G6", "G8", "H6", "H7", "H8"],
            ["G6", "G7", "G8", "H6", "H8"],
        ],
        [
            ["A5", "A7", "B5", "B6", "B7"],
            ["A5", "A6", "A7", "B5", "B7", "C5", "C6", "C7"],
            ["B5", "B6", "B7", "C5", "C7", "D5", "D6", "D7"],
            ["C5", "C6", "C7", "D5", "D7", "E5", "E6", "E7"],
            ["D5", "D6", "D7", "E5", "E7", "F5", "F6", "F7"],
            ["E5", "E6", "E7", "F5", "F7", "G5", "G6", "G7"],
            ["F5", "F6", "F7", "G5", "G7", "H5", "H6", "H7"],
            ["G5", "G6", "G7", "H5", "H7"],
        ],
        [
            ["A4", "A6", "B4", "B5", "B6"],
            ["A4", "A5", "A6", "B4", "B6", "C4", "C5", "C6"],
            ["B4", "B5", "B6", "C4", "C6", "D4", "D5", "D6"],
            ["C4", "C5", "C6", "D4", "D6", "E4", "E5", "E6"],
            ["D4", "D5", "D6", "E4", "E6", "F4", "F5", "F6"],
            ["E4", "E5", "E6", "F4", "F6", "G4", "G5", "G6"],
            ["F4", "F5", "F6", "G4", "G6", "H4", "H5", "H6"],
            ["G4", "G5", "G6", "H4", "H6"],
        ],
        [
            ["A3", "A5", "B3", "B4", "B5"],
            ["A3", "A4", "A5", "B3", "B5", "C3", "C4", "C5"],
            ["B3", "B4", "B5", "C3", "C5", "D3", "D4", "D5"],
            ["C3", "C4", "C5", "D3", "D5", "E3", "E4", "E5"],
            ["D3", "D4", "D5", "E3", "E5", "F3", "F4", "F5"],
            ["E3", "E4", "E5", "F3", "F5", "G3", "G4", "G5"],
            ["F3", "F4", "F5", "G3", "G5", "H3", "H4", "H5"],
            ["G3", "G4", "G5", "H3", "H5"],
        ],
        [
            ["A2", "A4", "B2", "B3", "B4"],
            ["A2", "A3", "A4", "B2", "B4", "C2", "C3", "C4"],
            ["B2", "B3", "B4", "C2", "C4", "D2", "D3", "D4"],
            ["C2", "C3", "C4", "D2", "D4", "E2", "E3", "E4"],
            ["D2", "D3", "D4", "E2", "E4", "F2", "F3", "F4"],
            ["E2", "E3", "E4", "F2", "F4", "G2", "G3", "G4"],
            ["F2", "F3", "F4", "G2", "G4", "H2", "H3", "H4"],
            ["G2", "G3", "G4", "H2", "H4"],
        ],
        [
            ["A1", "A3", "B1", "B2", "B3"],
            ["A1", "A2", "A3", "B1", "B3", "C1", "C2", "C3"],
            ["B1", "B2", "B3", "C1", "C3", "D1", "D2", "D3"],
            ["C1", "C2", "C3", "D1", "D3", "E1", "E2", "E3"],
            ["D1", "D2", "D3", "E1", "E3", "F1", "F2", "F3"],
            ["E1", "E2", "E3", "F1", "F3", "G1", "G2", "G3"],
            ["F1", "F2", "F3", "G1", "G3", "H1", "H2", "H3"],
            ["G1", "G2", "G3", "H1", "H3"],
        ],
        [
            ["A2", "B1", "B2"],
            ["A1", "A2", "B2", "C1", "C2"],
            ["B1", "B2", "C2", "D1", "D2"],
            ["C1", "C2", "D2", "E1", "E2"],
            ["D1", "D2", "E2", "F1", "F2"],
            ["E1", "E2", "F2", "G1", "G2"],
            ["F1", "F2", "G2", "H1", "H2"],
            ["G1", "G2", "H2"],
        ],
    ];

    // Test the getPossiblyMoves() method for all squares
    for (let x = 0; x < 8; x++) {
        for (let y = 0; y < 8; y++) {
            test(`should return the correct number of possible moves for square (${x}, ${y})`, () => {
                const king = new King(0);
                const currentCoordinates =  { x:x, y:y };
                let possibleMoves = king.getPossiblyMoves(currentCoordinates);

                const expectedMoves = expectedPossiblyMoves[x][y];

                expect(possibleMoves.map((coordonnates:coordonatesType)=>{
                    const position:positionType = BoardService.coordonatesToPosition(coordonnates);
                    return position.letter + position.number
                })).toEqual(expect.arrayContaining(expectedMoves));
            });
        }
    }

  // Test the isPromotable() method
  describe('isPromotable()', () => {
    it('should return false for isPromotable', () => {
      const king = new King(0);
      expect(king.isPromotable()).toBe(false);
    });
  });
});
import BoardService from "~/Service/board/BoardService";
import BoardPrinterService from "~/Service/board/BoardPrinterService";
import { positionType } from "~/Types/positionType";
import { coordonatesType } from "~/Types/coordonatesType";
import { Random } from "~/ToolsBox/random";
import Board from "~/Entity/Board";
import Pawn from "~/ChessGame/Piece/Pawn";
import Rook from "~/ChessGame/Piece/Rook";
import Knight from "~/ChessGame/Piece/Knight";
import Bishop from "~/ChessGame/Piece/Bishop";
import Queen from "~/ChessGame/Piece/Queen";
import King from "~/ChessGame/Piece/King";

describe('convertion des coordonnées : array 2D -> notation échiquier', () => {

    test('board[7][7] => square H1', () => {
        expect(BoardService.coordonatesToPosition({x:7,y:7})).toStrictEqual({letter:'H', number:'1'})
    });

    test('board[0][0] => square A8', () => {
        expect(BoardService.coordonatesToPosition({x:0,y:0})).toStrictEqual({letter:'A', number:'8'})
    });

    test('board[4][6] => square G4', () => {
        expect(BoardService.coordonatesToPosition({x:4,y:6})).toStrictEqual({letter:'G', number:'4'})
    });

    test('board[6][1] => square B2', () => {
        expect(BoardService.coordonatesToPosition({x:6,y:1})).toStrictEqual({letter:'B', number:'2'})
    });

});

describe('convertion des coordonnées : notation échiquier -> array 2D', () => {

    test('square H1 => board[7][7]', () => {
        expect(BoardService.positionToCoordonates({letter:'H',number:'1'})).toStrictEqual({x:7, y:7})
    });

    test('square h1 => board[7][7]', () => {
        expect(BoardService.positionToCoordonates({letter:'h',number:'1'})).toStrictEqual({x:7, y:7})
    });
    
    test('square A8 => board[0][0]', () => {
        expect(BoardService.positionToCoordonates({letter:'A',number:'8'})).toStrictEqual({x:0, y:0})
    });

    test('square G4 => board[4][6]', () => {
        expect(BoardService.positionToCoordonates({letter:'G',number:'4'})).toStrictEqual({x:4, y:6})
    });

    test('square B2 => board[6][1]', () => {
        expect(BoardService.positionToCoordonates({letter:'B',number:'2'})).toStrictEqual({x:6, y:1})
    });

});

describe('génération de board', () => {

    test('génération d\'un board vide', () => {
        const emptyBoard = BoardService.newBoard(true);
        expect(emptyBoard.squares).toHaveLength(8);

        for(const row of emptyBoard.squares)
        {
            expect(row).toHaveLength(8);
            for(const square of row)
            {
                expect(square).toBe(null);
            }
        }
    });

});

describe('ajout/suppression de pièce', () => {

    let board = BoardService.newBoard(true);
    const whiteOrBlack = Random.pickRandInt(0,2);
    const pieceWhiteOrBlack = Random.pickRandEntryOfArray([
        (new Pawn(whiteOrBlack)).toChar(),
        (new Rook(whiteOrBlack)).toChar(),
        (new Knight(whiteOrBlack)).toChar(),
        (new Bishop(whiteOrBlack)).toChar(),
        (new Queen(whiteOrBlack)).toChar(),
        (new King(whiteOrBlack)).toChar()
    ]);
    const piece = pieceWhiteOrBlack[Random.pickRandEntryOfArray([0,1])];

    const position:positionType = {
        letter:Random.pickRandLetterInRange('a', 'h'),
        number:Random.pickRandInt(1,9).toString()
    }

    const coordonates:coordonatesType = BoardService.positionToCoordonates(position);

    test('ajout d\'une pièce', () => {

        board.fillSquare(piece,coordonates);
        expect(board.squares[coordonates.x][coordonates.y]).toStrictEqual(piece);

    });

    test('retrait d\'une pièce', () => {

        board.emptySquare(coordonates);
        expect(board.squares[coordonates.x][coordonates.y]).toStrictEqual(null);

    });

});

describe('génération d\'un échiquier initialisé pour une nouvelle partie', () => {

    const board:Board = BoardService.newBoard();

    [0,1,2,3,4,5,6,7].forEach((coordonnate_x) => {
        expect((board.squares[6][coordonnate_x])!.toChar()).toStrictEqual((new Pawn(0)).toChar());
    });

    expect((board.squares[7][0])!.toChar()).toStrictEqual((new Rook(0)).toChar());
    expect((board.squares[7][1])!.toChar()).toStrictEqual((new Knight(0)).toChar());
    expect((board.squares[7][2])!.toChar()).toStrictEqual((new Bishop(0)).toChar());
    expect((board.squares[7][3])!.toChar()).toStrictEqual((new Queen(0)).toChar());
    expect((board.squares[7][4])!.toChar()).toStrictEqual((new King(0)).toChar());
    expect((board.squares[7][5])!.toChar()).toStrictEqual((new Bishop(0)).toChar());
    expect((board.squares[7][6])!.toChar()).toStrictEqual((new Knight(0)).toChar());
    expect((board.squares[7][7])!.toChar()).toStrictEqual((new Rook(0)).toChar());

    [0,1,2,3,4,5,6,7].forEach((coordonnate_x) => {
        expect((board.squares[1][coordonnate_x])!.toChar()).toStrictEqual((new Pawn(1)).toChar());
    });

    expect((board.squares[0][0])!.toChar()).toStrictEqual((new Rook(1)).toChar());
    expect((board.squares[0][1])!.toChar()).toStrictEqual((new Knight(1)).toChar());
    expect((board.squares[0][2])!.toChar()).toStrictEqual((new Bishop(1)).toChar());
    expect((board.squares[0][3])!.toChar()).toStrictEqual((new Queen(1)).toChar());
    expect((board.squares[0][4])!.toChar()).toStrictEqual((new King(1)).toChar());
    expect((board.squares[0][5])!.toChar()).toStrictEqual((new Bishop(1)).toChar());
    expect((board.squares[0][6])!.toChar()).toStrictEqual((new Knight(1)).toChar());
    expect((board.squares[0][7])!.toChar()).toStrictEqual((new Rook(1)).toChar());

});


describe('affichage echiquier sur le terminal', () => {

    const emptyBoard:Board = BoardService.newBoard(true)
    const board:Board = BoardService.newBoard();

     const expectedPrettyPrintEmptyBoard = 
        '8\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' + 
        '7\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '6\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '5\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '4\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '3\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '2\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '1\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '\n' +
        '\t A  B  C  D  E  F  G  H'
    ;

    const expectedPrettyPrintBoard = 
        '8\t[♖][♘][♗][♕][♔][♗][♘][♖]\n' + 
        '7\t[♙][♙][♙][♙][♙][♙][♙][♙]\n' +
        '6\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '5\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '4\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '3\t[ ][ ][ ][ ][ ][ ][ ][ ]\n' +
        '2\t[♟][♟][♟][♟][♟][♟][♟][♟]\n' +
        '1\t[♜][♞][♝][♛][♚][♝][♞][♜]\n' +
        '\n' +
        '\t A  B  C  D  E  F  G  H'
    ;    

    expect(BoardPrinterService.printToPostman(emptyBoard)).toStrictEqual(expectedPrettyPrintEmptyBoard);
    expect(BoardPrinterService.printToPostman(board)).toStrictEqual(expectedPrettyPrintBoard);
});

describe('loadBoardFromJsonString', ()=>{
    
});

describe('boardToJsonString', ()=>{
    
});

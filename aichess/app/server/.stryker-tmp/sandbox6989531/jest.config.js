/** @type {import('ts-jest').JestConfigWithTsJest} */
// @ts-nocheck

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageDirectory: 'reports/coverage',
  moduleNameMapper: {
    "~/(.*)": "<rootDir>/src/$1"
  }
};
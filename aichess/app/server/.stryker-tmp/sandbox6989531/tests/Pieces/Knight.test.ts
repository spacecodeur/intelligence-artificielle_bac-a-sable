// @ts-nocheck
import Knight from "~/ChessGame/Piece/Knight";

describe('Knight Class', () => {
  // Test the toChar() method
  describe('toChar()', () => {
    it('should return ♞ for player 0', () => {
      const knight = new Knight(0);
      expect(knight.toChar()).toBe('♞');
    });

    it('should return ♘ for player 1', () => {
      const knight = new Knight(1);
      expect(knight.toChar()).toBe('♘');
    });
  });

  // Define the expected number of possible moves for each square
  const expectedMoves = [
    [2, 3, 4, 4, 4, 4, 3, 2],
    [3, 4, 6, 6, 6, 6, 4, 3],
    [4, 6, 8, 8, 8, 8, 6, 4],
    [4, 6, 8, 8, 8, 8, 6, 4],
    [4, 6, 8, 8, 8, 8, 6, 4],
    [4, 6, 8, 8, 8, 8, 6, 4],
    [3, 4, 6, 6, 6, 6, 4, 3],
    [2, 3, 4, 4, 4, 4, 3, 2],
  ];

  // Test the getPossiblyMoves() method for all squares
  for (let x = 0; x < 8; x++) {
    for (let y = 0; y < 8; y++) {
      it(`should return the correct number of possible moves for square (${x}, ${y})`, () => {
        const knight = new Knight(0);
        const currentPosition = { x, y };
        const possibleMoves = knight.getPossiblyMoves(currentPosition);

        const expectedMoveCount = expectedMoves[x][y];

        expect(possibleMoves).toHaveLength(expectedMoveCount);
      });
    }
  }

  // Test the isPromotable() method
  describe('isPromotable()', () => {
    it('should return false for isPromotable', () => {
      const knight = new Knight(0);
      expect(knight.isPromotable()).toBe(false);
    });
  });
});
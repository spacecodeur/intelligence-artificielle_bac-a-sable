// @ts-nocheck
import Queen from "~/ChessGame/Piece/Queen";

describe('Queen Class', () => {
  // Test the toChar() method
  describe('toChar()', () => {
    it('should return ♛ for player 0', () => {
      const queen = new Queen(0);
      expect(queen.toChar()).toBe('♛');
    });

    it('should return ♕ for player 1', () => {
      const queen = new Queen(1);
      expect(queen.toChar()).toBe('♕');
    });
  });

  // Define the expected number of possible moves for each square
  const expectedMoves = [
    [21, 28, 28, 28, 28, 28, 28, 21],
    [28, 37, 37, 37, 37, 37, 37, 28],
    [28, 37, 44, 44, 44, 44, 37, 28],
    [28, 37, 44, 51, 51, 44, 37, 28],
    [28, 37, 44, 51, 51, 44, 37, 28],
    [28, 37, 44, 44, 44, 44, 37, 28],
    [28, 37, 37, 37, 37, 37, 37, 28],
    [21, 28, 28, 28, 28, 21, 28, 21],
  ];

  // Test the getPossiblyMoves() method for all squares
  for (let x = 0; x < 8; x++) {
    for (let y = 0; y < 8; y++) {
      it(`should return the correct number of possible moves for square (${x}, ${y})`, () => {
        const queen = new Queen(0);
        const currentPosition = { x, y };
        const possibleMoves = queen.getPossiblyMoves(currentPosition);

        const expectedMoveCount = expectedMoves[x][y];

        expect(possibleMoves).toHaveLength(expectedMoveCount);
      });
    }
  }

  // Test the isPromotable() method
  describe('isPromotable()', () => {
    it('should return false for isPromotable', () => {
      const queen = new Queen(0);
      expect(queen.isPromotable()).toBe(false);
    });
  });
});

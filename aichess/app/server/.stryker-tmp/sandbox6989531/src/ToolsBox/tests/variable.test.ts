// @ts-nocheck
import { Variable } from '../variable';

describe('check functions', () => {
    test('function isAnInteger', () => {
        expect(Variable.isAnInteger(10)).toBe(true);
        expect(Variable.isAnInteger(-10)).toBe(true);
        expect(Variable.isAnInteger(5.5)).toBe(false);
    });

    test('function isAChar', () => {
        expect(Variable.isAChar('a')).toBe(true);
        expect(Variable.isAChar('5')).toBe(true);
        expect(Variable.isAChar('foo')).toBe(false);
    });

    test('function isALetter', () => {
        [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
        ].forEach((letter) => {
            expect(Variable.isALetter(letter)).toBe(true);
        });

        expect(Variable.isALetter('5')).toBe(false);
        expect(Variable.isALetter('(')).toBe(false);
    });

    test('function isALetter', () => {
        expect(Variable.isALetter('a')).toBe(true);
        expect(Variable.isALetter('5')).toBe(false);
        expect(Variable.isALetter('(')).toBe(false);
    });

    test('function areEqual', () => {
        expect(Variable.areEqual('foo', 'foo')).toBe(true);
        expect(Variable.areEqual('foo', 'faa')).toBe(false);
        expect(Variable.areEqual('faa', 'foo')).toBe(false);
        expect(Variable.areEqual('10', '10')).toBe(true);
        expect(Variable.areEqual('10', 10)).toBe(false);
        expect(Variable.areEqual(10.5, '10.5')).toBe(false);
    });

    test('function areSameType', () => {
        expect(Variable.areSameType('foo', 'foo')).toBe(true);
        expect(Variable.areSameType('foo', 'faa')).toBe(true);
        expect(Variable.areSameType('10', '10')).toBe(true);
        expect(Variable.areSameType('10', 10)).toBe(false);
        expect(Variable.areSameType(7, 7)).toBe(true);
        expect(Variable.areSameType(true, false)).toBe(true);
    });

    test('function isLessThan', () => {
        expect(Variable.isLessThan('aaa', 'aab')).toBe(true);
        expect(Variable.isLessThan('aab', 'aaa')).toBe(false);
        expect(Variable.isLessThan('aaa', 'aaa')).toBe(false);
        expect(Variable.isLessThan('10', '99')).toBe(true);
        expect(Variable.isLessThan('10', '10')).toBe(false);
        expect(Variable.isLessThan(10, 99)).toBe(true);
        expect(Variable.isLessThan(10, 10)).toBe(false);
        expect(Variable.isLessThan('99', '10')).toBe(false);
        expect(Variable.isLessThan(99, 10)).toBe(false);
    });

    test('function isLessThanOrEqual', () => {
        expect(Variable.isLessThanOrEqual('aaa', 'aab')).toBe(true);
        expect(Variable.isLessThanOrEqual('aab', 'aaa')).toBe(false);
        expect(Variable.isLessThanOrEqual('aaa', 'aaa')).toBe(true);
        expect(Variable.isLessThanOrEqual('10', '99')).toBe(true);
        expect(Variable.isLessThanOrEqual('10', '10')).toBe(true);
        expect(Variable.isLessThanOrEqual(10, 99)).toBe(true);
        expect(Variable.isLessThanOrEqual(10, 10)).toBe(true);
        expect(Variable.isLessThanOrEqual('99', '10')).toBe(false);
        expect(Variable.isLessThanOrEqual(99, 10)).toBe(false);
    });

    test('function mustBeAnIntegerGreaterThan', () => {
        expect(() => {
            Variable.mustBeAnIntegerGreaterThan(0, 0);
        }).toThrowError(new TypeError('the integer 0 is not greater than 0'));

        expect(() => {
            Variable.mustBeAnIntegerGreaterThan(-1, 0);
        }).toThrowError(new TypeError('the integer -1 is not greater than 0'));

        expect(Variable.mustBeAnIntegerGreaterThan(1, 0)).toBe(true);
    });

    test('function mustBeAnInteger', () => {
        expect(() => {
            Variable.mustBeAnInteger(5.5);
        }).toThrowError(
            'the parameter is not an integer as expected (currently value is : 5.5 )'
        );

        expect(Variable.mustBeAnInteger(5)).toBe(true);
    });

    test('function mustBeAChar', () => {
        expect(() => {
            Variable.mustBeAChar('foo');
        }).toThrowError(
            'the parameter is not a char as expected (currently value is : foo )'
        );

        expect(Variable.mustBeAnInteger(5)).toBe(true);
    });

    test('function mustBeALetter', () => {
        expect(() => {
            Variable.mustBeALetter('5');
        }).toThrowError('the parameter with the value \'5\' is not a letter');

        expect(Variable.mustBeALetter('a')).toBe(true);
    });

    test('function mustBeEqual', () => {
        expect(() => {
            Variable.mustBeEqual('foo', 'faa');
        }).toThrowError(
            'parameter1 and parameter2 are not equal as expected : foo (string) !== faa (string)'
        );

        expect(Variable.mustBeEqual('foo', 'foo')).toBe(true);
    });

    test('function mustBeSameType', () => {
        expect(() => {
            Variable.mustBeSameType('10', 10);
        }).toThrowError(
            'parameter1 and parameter2 are not the same type : typeof parameter1 (string) !== typeof parameter2 (number)'
        );

        expect(Variable.mustBeSameType(true, false)).toBe(true);
    });

    test('function mustBeLessThan', () => {
        expect(() => {
            Variable.mustBeLessThan('aaa', 'aaa');
        }).toThrowError('parameter1 (aaa) is not lower than parameter2 (aaa)');

        expect(Variable.mustBeLessThan('10', '99')).toBe(true);
    });

    test('function mustBeLessThanOrEqual', () => {
        expect(() => {
            Variable.mustBeLessThanOrEqual('aab', 'aaa');
        }).toThrowError(
            'parameter1 (aab) is not lower (or equal) than parameter2 (aaa)'
        );

        expect(Variable.mustBeLessThanOrEqual(10, 99)).toBe(true);
    });

    test('function mustBeNonEmptyObject', () => {
        expect(() => {
            Variable.mustBeNonEmptyObject({});
        }).toThrowError('The object is empty');

        expect(Variable.mustBeNonEmptyObject({ iAm: 'notEmptyObject' })).toBe(
            true
        );
    });
});

// @ts-nocheck
export type moveType = {
    start: {
        position: string
    },
    end: {
        position: string,
        promotion: string
    }
};
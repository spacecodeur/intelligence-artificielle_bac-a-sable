// @ts-nocheck
export type coordonatesType = {
    x: number;
    y: number;
};

// @ts-nocheck
import { Request, Response } from "express";
import Board from "~/Entity/Board";
import MoveService from "~/Service/MoveService";
import BoardService from "~/Service/board/BoardService";
import { moveType } from "~/Types/moveType";

const makeMoveAction = (request: Request, response: Response) => {

    let move:moveType = MoveService.stringMoveToObjectMove(request.body.move);
    let board:Board = BoardService.loadBoardFromJson(JSON.parse(request.body.board));

    // 

    return response.send('coucou');

}

export default makeMoveAction;
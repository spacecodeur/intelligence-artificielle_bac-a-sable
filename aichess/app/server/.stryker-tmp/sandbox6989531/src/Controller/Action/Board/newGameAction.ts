// @ts-nocheck
import { Request, Response } from "express";
import Board from "~/Entity/Board";
import BoardService from "~/Service/board/BoardService";
import BoardPrinterService from "~/Service/board/BoardPrinterService";

const newGameAction = (request: Request, response: Response) => {
    const board:Board = BoardService.newBoard();
    response.send(BoardPrinterService.printToPostman(board));
}

export default newGameAction;
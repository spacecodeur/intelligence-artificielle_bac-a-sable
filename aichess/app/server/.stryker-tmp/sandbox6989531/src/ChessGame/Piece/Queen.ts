// @ts-nocheck
import PieceAbstract from '../PieceAbstract';
import { coordonatesType } from '~/Types/coordonatesType';

class Queen extends PieceAbstract {
    constructor(player: number) {
        super(player);
    }

    toChar(): string {
        return this.getPlayer() === 0 ? '♛' : '♕';
    }

    getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[] {
        // Implement the logic for possible moves of the Queen
        const possibleMoves: coordonatesType[] = [];

        // Horizontal and vertical moves
        for (let x = 0; x < 8; x++) {
            if (x !== currentCoordinate.x) {
                possibleMoves.push({ x, y: currentCoordinate.y });
            }
        }
        for (let y = 0; y < 8; y++) {
            if (y !== currentCoordinate.y) {
                possibleMoves.push({ x: currentCoordinate.x, y });
            }
        }

        // Diagonal moves
        for (let i = -7; i <= 7; i++) {
            if (i !== 0 && currentCoordinate.x + i >= 0 && currentCoordinate.x + i < 8 && currentCoordinate.y + i >= 0 && currentCoordinate.y + i < 8) {
                possibleMoves.push({ x: currentCoordinate.x + i, y: currentCoordinate.y + i });
            }
            if (i !== 0 && currentCoordinate.x + i >= 0 && currentCoordinate.x + i < 8 && currentCoordinate.y - i >= 0 && currentCoordinate.y - i < 8) {
                possibleMoves.push({ x: currentCoordinate.x + i, y: currentCoordinate.y - i });
            }
        }

        // You may need to add further checks to ensure the Queen doesn't go out of bounds or move through other pieces.

        return possibleMoves;
    }

    isPromotable(): boolean {
        return false;
    }
}

export default Queen;

// @ts-nocheck
import { moveType } from "~/Types/moveType";

export default class MoveService
{

    static moveRegex = /([a-h][1-8])-([a-h][1-8])([QRBN])?/;


    public static isWellFormedStringMove(stringMove:string): boolean 
    {
        return MoveService.moveRegex.test(stringMove);
    }

    public static stringMoveToObjectMove(stringMove:string): moveType
    {
        let oMove = stringMove.match(MoveService.moveRegex);
        return {
            start: {
                position: oMove![1]
            },
            end: {
                position: oMove![2],
                promotion: oMove![3]
            }
        }
    }

    public static isAllowedMove(stringBoard: string, ): boolean
    {
        return true;
    }


}
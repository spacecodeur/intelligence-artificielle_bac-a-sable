// @ts-nocheck
import type { positionType } from '~/Types/positionType';
import type { coordonatesType } from '~/Types/coordonatesType';

import Board from '~/Entity/Board';
import { squaresType } from '~/Types/squaresType';
import Pawn from '~/ChessGame/Piece/Pawn';
import Rook from '~/ChessGame/Piece/Rook';
import Knight from '~/ChessGame/Piece/Knight';
import Bishop from '~/ChessGame/Piece/Bishop';
import Queen from '~/ChessGame/Piece/Queen';
import King from '~/ChessGame/Piece/King';


export default class BoardService {

    static chessBoardCoor = {
        x: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
        y: ['8', '7', '6', '5', '4', '3', '2', '1'],
    }

    public static positionToCoordonates(position: positionType): coordonatesType 
    {
        return {
            x: this.chessBoardCoor.y.indexOf(position.number),
            y: this.chessBoardCoor.x.indexOf(position.letter.toUpperCase())
        }
    }

    public static coordonatesToPosition(coordonates: coordonatesType): positionType 
    {
        return {
            letter: this.chessBoardCoor.x[coordonates.y],
            number: this.chessBoardCoor.y[coordonates.x]
        }
    }

    public static newBoard(buildEmptyBoard:boolean = false): Board
    {

        let eBoard = new Board([
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
        ]);

        if(true === buildEmptyBoard)
        {
            return eBoard;
        }
        else
        {
            // on place les pions
            ['2', '7'].forEach((number, player) => {
                ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Pawn(player), 
                        BoardService.positionToCoordonates({letter, number})
                    )
                });
            });
    
            // on place les pièces
            ['1', '8'].forEach((number, player) => {
                ['a', 'h'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Rook(player), 
                        BoardService.positionToCoordonates({letter, number})
                    );
                });
                ['b', 'g'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Knight(player), 
                        BoardService.positionToCoordonates({letter, number})
                    );
                });
                ['c', 'f'].forEach((letter) => {
                    eBoard.fillSquare(
                        new Bishop(player), 
                        BoardService.positionToCoordonates({letter, number})
                    );
                });
                eBoard.fillSquare(
                    new Queen(player),
                    BoardService.positionToCoordonates({letter: 'd', number})
                );
                eBoard.fillSquare(
                    new King(player),
                    BoardService.positionToCoordonates({letter: 'e', number})
                );
            });

            return eBoard;
        }
    }

    public static loadBoardFromJson(squares: squaresType): Board
    {
        return new Board(squares);
    }
}
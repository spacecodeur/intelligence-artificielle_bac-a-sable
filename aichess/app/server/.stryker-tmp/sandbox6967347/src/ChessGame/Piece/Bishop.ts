// @ts-nocheck
import PieceAbstract from '../PieceAbstract';
import { coordonatesType } from '~/Types/coordonatesType';

class Bishop extends PieceAbstract {
    constructor(player: number) {
        super(player);
    }

    toChar(): string {
        return this.getPlayer() === 0 ? '♝' : '♗';
    }

    getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[] {
        // Implement the logic for possible moves of the Bishop
        // The Bishop can move diagonally in any direction
        const possibleMoves: coordonatesType[] = [];
        
        // Diagonal up-left
        let x = currentCoordinate.x - 1;
        let y = currentCoordinate.y - 1;
        while (x >= 0 && y >= 0) {
            possibleMoves.push({ x, y });
            x--;
            y--;
        }
        
        // Diagonal up-right
        x = currentCoordinate.x - 1;
        y = currentCoordinate.y + 1;
        while (x >= 0 && y < 8) {
            possibleMoves.push({ x, y });
            x--;
            y++;
        }
        
        // Diagonal down-left
        x = currentCoordinate.x + 1;
        y = currentCoordinate.y - 1;
        while (x < 8 && y >= 0) {
            possibleMoves.push({ x, y });
            x++;
            y--;
        }
        
        // Diagonal down-right
        x = currentCoordinate.x + 1;
        y = currentCoordinate.y + 1;
        while (x < 8 && y < 8) {
            possibleMoves.push({ x, y });
            x++;
            y++;
        }

        // You may need to add further checks to ensure the Bishop doesn't go out of bounds or move through other pieces.

        return possibleMoves;
    }

    isPromotable(): boolean {
        // Bishops are not promotable in standard chess rules
        return false;
    }
}

export default Bishop;

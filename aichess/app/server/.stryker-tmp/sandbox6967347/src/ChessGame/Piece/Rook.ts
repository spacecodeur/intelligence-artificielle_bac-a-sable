// @ts-nocheck
import PieceAbstract from '../PieceAbstract';
import { coordonatesType } from '~/Types/coordonatesType';

class Rook extends PieceAbstract {
    constructor(player: number) {
        super(player);
    }

    toChar(): string {
        return this.getPlayer() === 0 ? '♜' : '♖';
    }

    getPossiblyMoves(currentCoordinate: coordonatesType): coordonatesType[] {
        // Implement the logic for possible moves of the Rook
        const possibleMoves: coordonatesType[] = [];

        // Horizontal moves
        for (let x = 0; x < 8; x++) {
            if (x !== currentCoordinate.x) {
                possibleMoves.push({ x, y: currentCoordinate.y });
            }
        }

        // Vertical moves
        for (let y = 0; y < 8; y++) {
            if (y !== currentCoordinate.y) {
                possibleMoves.push({ x: currentCoordinate.x, y });
            }
        }

        return possibleMoves;
    }

    isPromotable(): boolean {
        return false;
    }
}

export default Rook;

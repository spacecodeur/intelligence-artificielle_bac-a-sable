// @ts-nocheck
import { Request, Response, NextFunction } from "express";
import MoveService from "~/Service/MoveService";
import { Variable } from "~/ToolsBox/variable";


const requestParamPlayerCheck = (request: Request, response: Response, next: NextFunction): void|Response => 
{
    try
    {
        Variable.mustBeDefined(request.body.player);
    }
    catch (e)
    {
        if(e instanceof TypeError)
        {
            return response.status(400).send('player param missing');
        }
        else
        {
            throw Error('Exception trigged unexpected');
        }
    }

    try
    {
        Variable.mustBeNumericInteger(request.body.player);

        if(false === [0,1].includes(request.body.player))
        {
            return response.status(400).send('bad player param format');
        }
    }
    catch(e)
    {
        if(e instanceof TypeError)
        {
            return response.status(400).send('move param missing');
        }
        else
        {
            throw Error('Exception trigged unexpected');
        }
    }

    return next();
}

export default requestParamPlayerCheck;
// @ts-nocheck
import { Router } from 'express';
import newGame from './Action/Board/newGameAction';

import makeMoveAction from './Action/Board/makeMoveAction';
import makeMoveValidator from './Validator/makeMoveValidator';


const router = Router();

// Board Actions
router.get( '/',                            newGame);
router.post('/move', ...makeMoveValidator , makeMoveAction); // not warAction

export default router;
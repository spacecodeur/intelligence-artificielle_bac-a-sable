// @ts-nocheck
export type positionType = {
    letter: string;
    number: string;
};

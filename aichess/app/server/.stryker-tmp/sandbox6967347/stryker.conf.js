/**
* @type {import('@stryker-mutator/api/core').StrykerOptions}
*/
// @ts-nocheck

module.exports = {
    mutate: [
      '!**/*.ts',
      '!**/*.js',
      'services/**/*.ts',
      'utils/**/*.ts'
    ],
    testRunner: 'jest',
    jest: {
        "projectType": "custom",
        "enableFindRelatedTests": true
    },
    reporters: ['progress', 'clear-text', 'html'],
    coverageAnalysis: 'perTest',
    checkers: ['typescript']
  };
// @ts-nocheck
import King from "~/ChessGame/Piece/King";

describe('King Class', () => {
  // Test the toChar() method
  describe('toChar()', () => {
    it('should return ♚ for player 0', () => {
      const king = new King(0);
      expect(king.toChar()).toBe('♚');
    });

    it('should return ♔ for player 1', () => {
      const king = new King(1);
      expect(king.toChar()).toBe('♔');
    });
  });

  // Define the expected number of possible moves for each square
  const expectedMoves = [
    [3, 5, 5, 5, 5, 5, 5, 3],
    [5, 8, 8, 8, 8, 8, 8, 5],
    [5, 8, 8, 8, 8, 8, 8, 5],
    [5, 8, 8, 8, 8, 8, 8, 5],
    [5, 8, 8, 8, 8, 8, 8, 5],
    [5, 8, 8, 8, 8, 8, 8, 5],
    [5, 8, 8, 8, 8, 8, 8, 5],
    [3, 5, 5, 5, 5, 5, 5, 3],
  ];

  // Test the getPossiblyMoves() method for all squares
  for (let x = 0; x < 8; x++) {
    for (let y = 0; y < 8; y++) {
      it(`should return the correct number of possible moves for square (${x}, ${y})`, () => {
        const king = new King(0);
        const currentPosition = { x, y };
        const possibleMoves = king.getPossiblyMoves(currentPosition);

        const expectedMoveCount = expectedMoves[x][y];

        expect(possibleMoves).toHaveLength(expectedMoveCount);
      });
    }
  }

  // Test the isPromotable() method
  describe('isPromotable()', () => {
    it('should return false for isPromotable', () => {
      const king = new King(0);
      expect(king.isPromotable()).toBe(false);
    });
  });
});
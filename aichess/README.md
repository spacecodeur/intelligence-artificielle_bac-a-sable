# AICHESS

Une application maison développée pour plusieurs objectifs : 
- me faire la main sur certaines technos
- me créer un bac à sable (un jeu d'échec) pour pouvoir développer ma toute première IA
- servir de gros tuto pour les personnes intéressées par :
    - des stack tech sexy : express, typescript, react, ...
    - coder un jeu d'échec, c'est cool les échecs (si si)
    - comment gérer une application complète client-serveur
    - la contruction d'une IA (machin learning toussa)

## Architecture générale

L'application est découpée dans trois principaux dossiers : 

- **`/ai`** : contient le code de l'intelligence artificielle qui fera ofice d'adversaire
- **`/app`** : contient le code du jeu d'échec, ce dossier est divisé en deux parties client et serveur
    - **`/app/server`** : le traitement et stockage des données du jeu, la logique du jeu (déplacement des pièces, etc...)
    - **`/app/client`** : la gestion de la vue de l'application (interface graphique)

![doc/architecture.jpg](doc/architecture.jpg)

Pour plus de détails sur chacune des parties, se référer aux `README.md` présents dans les dossiers : `/ai`, `/app/server` et `/ai/client`

